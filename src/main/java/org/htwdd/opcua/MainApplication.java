package org.htwdd.opcua;

import java.io.IOException;
import java.net.URISyntaxException;

import org.xml.sax.SAXException;

import com.prosysopc.ua.ModelException;
import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.SessionActivationException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.client.ConnectException;
import com.prosysopc.ua.client.InvalidServerEndpointException;
import com.prosysopc.ua.server.InvalidTypeNodeException;
import com.prosysopc.ua.server.NodeBuilderException;
import com.prosysopc.ua.server.UaServerException;

public class MainApplication {

	
	public static void main(String[] args) throws UaServerException, URISyntaxException, InvalidServerEndpointException, ConnectException, SessionActivationException, ServiceException, SAXException, IOException, ModelException, InvalidTypeNodeException, StatusException, IllegalArgumentException, NodeBuilderException {
		
	}
	
	public static void log(Object o) {
		System.out.println(o);
	}
}
