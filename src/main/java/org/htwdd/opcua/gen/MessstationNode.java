// Generated from SampleTypes
// by Prosys OPC UA Java SDK Codegen
//
package org.htwdd.opcua.gen;

import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.TypeDefinitionId;
import com.prosysopc.ua.server.NodeManagerUaNode;
import com.prosysopc.ua.server.ServiceContext;
import com.prosysopc.ua.stack.builtintypes.LocalizedText;
import com.prosysopc.ua.stack.builtintypes.NodeId;
import com.prosysopc.ua.stack.builtintypes.QualifiedName;
import com.prosysopc.ua.stack.core.EventNotifierType;
import com.prosysopc.ua.stack.core.StatusCodes;
import com.prosysopc.ua.types.opcua.BaseEventType;
import com.prosysopc.ua.types.opcua.server.BaseEventTypeNode;

import java.lang.Override;

/**
 * Generated on 2020-04-01 19:10:45
 */
@TypeDefinitionId("nsu=http://yourorganisation.org/STTI/;i=1002")
public class MessstationNode extends MessstationNodeBase {
	
  protected MessstationNode(NodeManagerUaNode nodeManager, NodeId nodeId, QualifiedName browseName,
      LocalizedText displayName) {
    super(nodeManager, nodeId, browseName, displayName);
    BaseEventTypeNode et = nodeManager.createInstance(BaseEventTypeNode.class,"lol");
   
  }

  @Override
  public void afterCreate() {
    // Use this method to initialize the nodes, when they are all created.
    // Note that 'super.afterCreate()' performs default initializations, so consider
    // whether your own initializations should be done before or after it.
    super.afterCreate();
  }

  @Override
  protected void ongetTemperatur(ServiceContext serviceContext) throws StatusException {
    System.out.println(this.getTemperatur());
   
	
    
  }
}
