// Generated from SampleTypes
// by Prosys OPC UA Java SDK Codegen
//
package org.htwdd.opcua.gen;

import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.TypeDefinitionId;
import com.prosysopc.ua.nodes.Mandatory;
import com.prosysopc.ua.nodes.UaMethod;
import com.prosysopc.ua.nodes.UaVariable;
import com.prosysopc.ua.server.GeneratedNodeInitializer;
import com.prosysopc.ua.server.NodeManagerUaNode;
import com.prosysopc.ua.server.ServiceContext;
import com.prosysopc.ua.stack.builtintypes.DiagnosticInfo;
import com.prosysopc.ua.stack.builtintypes.LocalizedText;
import com.prosysopc.ua.stack.builtintypes.NodeId;
import com.prosysopc.ua.stack.builtintypes.QualifiedName;
import com.prosysopc.ua.stack.builtintypes.StatusCode;
import com.prosysopc.ua.stack.builtintypes.Variant;
import com.prosysopc.ua.types.opcua.server.BaseDataVariableTypeNode;
import com.prosysopc.ua.types.opcua.server.BaseObjectTypeNode;
import java.lang.Double;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import org.htwdd.opcua.gen.Messstation;

/**
 * Generated on 2020-04-01 19:10:45
 */
@TypeDefinitionId("nsu=http://yourorganisation.org/STTI/;i=1002")
public abstract class MessstationNodeBase extends BaseObjectTypeNode implements Messstation {
  private static GeneratedNodeInitializer<MessstationNode> messstationNodeInitializer;

  private static MessstationgetTemperaturMethod getTemperaturMethodImplementation;

  protected MessstationNodeBase(NodeManagerUaNode nodeManager, NodeId nodeId,
      QualifiedName browseName, LocalizedText displayName) {
    super(nodeManager, nodeId, browseName, displayName);
  }

  @Override
  public void afterCreate() {
    super.afterCreate();

    // Call afterCreate for each sub-node (if the node has any)
    callAfterCreateIfExists(getTemperaturNode());
    GeneratedNodeInitializer<MessstationNode> impl = getMessstationNodeInitializer();
    if(impl != null) {
      impl.init((MessstationNode)this);
    }
  }

  public static GeneratedNodeInitializer<MessstationNode> getMessstationNodeInitializer() {
    return messstationNodeInitializer;
  }

  public static void setMessstationNodeInitializer(GeneratedNodeInitializer<MessstationNode> messstationNodeInitializerNewValue) {
    messstationNodeInitializer=messstationNodeInitializerNewValue;
  }

  @Mandatory
  @Override
  public BaseDataVariableTypeNode getTemperaturNode() {
    QualifiedName browseName = getQualifiedName("http://yourorganisation.org/STTI/", "Temperatur");
    return (BaseDataVariableTypeNode) getComponent(browseName);
  }

  @Mandatory
  @Override
  public Double getTemperatur() {
    UaVariable node = getTemperaturNode();
    if (node == null) {
      throw new RuntimeException("Mandatory node Temperatur does not exist");
    }
    Object value = node.getValue().getValue().getValue();
    return (Double) value;
  }

  @Mandatory
  @Override
  public void setTemperatur(Double value) {
    UaVariable node = getTemperaturNode();
    if (node == null) {
      throw new RuntimeException("Setting Temperatur failed: does not exist (Optional Nodes must be configured in NodeBuilder)");
    }
    try {
      node.setValue(value);
    } catch(StatusException e) {
      throw new RuntimeException("Setting Temperatur failed unexpectedly", e);
    }
  }

  @Override
  public Variant[] callMethod(ServiceContext serviceContext, NodeId methodId,
      Variant[] inputArguments, StatusCode[] inputArgumentResults,
      DiagnosticInfo[] inputArgumentDiagnosticInfos) throws StatusException {
    if (isComponentMatch(getQualifiedName("http://yourorganisation.org/STTI/", "getTemperatur"), methodId)) {
      dogetTemperatur(serviceContext);
      return null;
    }
    return super.callMethod(serviceContext, methodId, inputArguments, inputArgumentResults, inputArgumentDiagnosticInfos);
  }

  @Mandatory
  @Override
  public UaMethod getgetTemperaturNode() {
    QualifiedName browseName = getQualifiedName("http://yourorganisation.org/STTI/", "getTemperatur");
    return (UaMethod) getComponent(browseName);
  }

  protected abstract void ongetTemperatur(ServiceContext serviceContext) throws StatusException;

  
  private void dogetTemperatur(ServiceContext serviceContext) throws StatusException {
    MessstationgetTemperaturMethod impl = getGetTemperaturMethodImplementation();
    if(impl != null) {
      impl.getTemperatur(serviceContext, (MessstationNode)this);
    } else {
      ongetTemperatur(serviceContext);
    }
  }

  public static MessstationgetTemperaturMethod getGetTemperaturMethodImplementation() {
    return getTemperaturMethodImplementation;
  }

  public static void setGetTemperaturMethodImplementation(MessstationgetTemperaturMethod getTemperaturMethodImplementationNewValue) {
    getTemperaturMethodImplementation=getTemperaturMethodImplementationNewValue;
  }
}
