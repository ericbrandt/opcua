// Generated from SampleTypes
// by Prosys OPC UA Java SDK Codegen
//
package org.htwdd.opcua.gen;

import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.TypeDefinitionId;
import com.prosysopc.ua.nodes.Mandatory;
import com.prosysopc.ua.nodes.UaMethod;
import com.prosysopc.ua.types.opcua.BaseDataVariableType;
import com.prosysopc.ua.types.opcua.BaseObjectType;
import java.lang.Double;
import java.lang.String;

/**
 * Generated on 2020-04-01 19:10:45
 */
@TypeDefinitionId("nsu=http://yourorganisation.org/STTI/;i=1002")
public interface Messstation extends BaseObjectType {
  String TEMPERATUR = "Temperatur";

  String GET_TEMPERATUR = "getTemperatur";

  @Mandatory
  BaseDataVariableType getTemperaturNode();

  @Mandatory
  Double getTemperatur();

  @Mandatory
  void setTemperatur(Double value) throws StatusException;

  @Mandatory
  UaMethod getgetTemperaturNode();

}
