// Generated from SampleTypes
// by Prosys OPC UA Java SDK Codegen
//
package org.htwdd.opcua.gen;

import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.server.ServiceContext;

public abstract interface MessstationgetTemperaturMethod {
  Double getTemperatur(ServiceContext serviceContext, MessstationNode node) throws StatusException;
}
