// Generated from SampleTypes
// by Prosys OPC UA Java SDK Codegen
//
package org.htwdd.opcua.gen;

import com.prosysopc.ua.stack.builtintypes.ExpandedNodeId;

/**
 * Generated on 2020-04-01 19:10:45
 */
public interface ObjectTypeIds {
  ExpandedNodeId Messstation = ObjectTypeIdsInit.initMessstation();
}
