package org.htwdd.opcua.client;

import com.prosysopc.ua.client.UaClient;

public interface Connector {
	public UaClient connect(DemoClient demo);
}
