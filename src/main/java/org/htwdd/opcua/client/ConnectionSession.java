package org.htwdd.opcua.client;

import java.net.URISyntaxException;

import com.prosysopc.ua.ServiceException;
import com.prosysopc.ua.client.UaClient;
import com.prosysopc.ua.stack.transport.security.SecurityMode;

public class ConnectionSession implements DemoClient{

	private final String serverUri;
	
	public ConnectionSession(final String serverUri) {
		this.serverUri = serverUri;
	}
	
	public UaClient configure() {
		//TODO: return a client which is configured with serverUri and lowest security mode
		
		return null;
	}	
	
	
}
