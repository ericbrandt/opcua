package org.htwdd.opcua.client;

import com.prosysopc.ua.client.UaClient;

public interface DemoClient {
	public UaClient configure();
}
