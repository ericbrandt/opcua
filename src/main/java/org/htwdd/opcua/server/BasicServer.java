package org.htwdd.opcua.server;

import com.prosysopc.ua.server.UaServer;

public class BasicServer implements DemoServer {

	private final int port;
	private final String serverName;
	
	public BasicServer(
			final int port,
			final String serverName
			) {
		this.port = port;
		this.serverName = serverName;		
	}
	
	@Override
	public UaServer configure() {
		return null;		
	}

}
