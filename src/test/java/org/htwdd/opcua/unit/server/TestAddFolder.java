package org.htwdd.opcua.unit.server;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.htwdd.opcua.server.BasicServer;
import org.htwdd.opcua.server.util.AddFolder;
import org.junit.jupiter.api.Test;

import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.server.NodeManagerUaNode;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;
import com.prosysopc.ua.types.opcua.server.FolderTypeNode;
import static org.mockito.Mockito.*;
public class TestAddFolder {

	
	@Test
	public void FolderShouldBeAddedToCustomNamespace() throws StatusException, UaServerException {
		//given a configured server		
		BasicServer basicServer = new BasicServer(55555,"test");
		UaServer server = basicServer.configure();
		server.init();
		//when a folder is added to the address space by AddFolder class
		NodeManagerUaNode nodeManager = new NodeManagerUaNode(server, "https://htwdd.de/info");	
		AddFolder addFolder = new AddFolder(nodeManager);
		FolderTypeNode node = addFolder.addFolder(server, "MyFolder");		
		//then address space should contain it
		String name = server.getAddressSpace().getNode(node.getNodeId()).getBrowseName().getName();
		assertEquals("MyFolder", name);
		
	}
}
