package org.htwdd.opcua.unit.server;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.htwdd.opcua.gen.Messstation;
import org.htwdd.opcua.gen.ServerInformationModel;
import org.htwdd.opcua.server.BasicServer;
import org.htwdd.opcua.server.UmweltStationXML;
import org.junit.jupiter.api.Test;

import com.prosysopc.ua.server.UaServer;

public class TestAddInformationModel {

	@Test
	public void ServerShouldContainImportedTypes() {
		final String STATION_NAME = "TestStation";
		BasicServer basicServer = new BasicServer(50000, "TestServer");		
		UmweltStationXML xmlStation = new UmweltStationXML(basicServer);
		UaServer server = xmlStation.ImportXMLModel("stti.xml");
		
		
		Messstation m = server.getNodeManagerRoot().createInstance(Messstation.class, STATION_NAME);
		assertEquals(STATION_NAME,m.getDisplayName().getText());
		
		
		
	}
	
}
