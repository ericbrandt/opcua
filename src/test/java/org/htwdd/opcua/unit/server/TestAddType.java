package org.htwdd.opcua.unit.server;

import org.htwdd.opcua.server.BasicServer;
import org.htwdd.opcua.server.util.AddType;
import org.junit.jupiter.api.Test;

import com.prosysopc.ua.StatusException;
import com.prosysopc.ua.nodes.UaObjectType;
import com.prosysopc.ua.server.NodeManagerUaNode;
import com.prosysopc.ua.server.UaServer;
import com.prosysopc.ua.server.UaServerException;
import com.prosysopc.ua.stack.builtintypes.NodeId;

public class TestAddType {

	
	@Test
	public void TypeShouldBeAddedToAddressSpace() throws UaServerException {
		BasicServer basicServer = new BasicServer(55555,"test");
		UaServer server = basicServer.configure();
		server.init();
		NodeManagerUaNode nodeManager = new NodeManagerUaNode(server, "https://htwdd.de/info");	
		AddType addType = new AddType(nodeManager);
		UaObjectType type = addType.add(server, "MyType");
		NodeId nodeId = type.getNodeId();
		server.getAddressSpace().getNode(nodeId);

		
	}
}
