package org.htwdd.opcua.unit.client;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.htwdd.opcua.client.ConnectionSession;
import org.htwdd.opcua.client.ConnectionUserPassword;
import org.htwdd.opcua.client.DemoClient;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.*;

import com.prosysopc.ua.SessionActivationException;
import com.prosysopc.ua.client.UaClient;


public class TestUserPasswordAuth {
	
	@Test
	public void AccessShouldBeGrantedOnlyWithPassword() throws SessionActivationException {
		final String USER = "alf";
		DemoClient session = mock(ConnectionSession.class);
		when(session.configure()).thenReturn(new UaClient());
		
		DemoClient con = new ConnectionUserPassword(session, USER, "melmac");		
		UaClient client= con.configure();
		assertEquals(USER, client.getUserIdentity().getName());
	}
}
